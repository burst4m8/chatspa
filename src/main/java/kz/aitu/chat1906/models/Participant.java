package kz.aitu.chat1906.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "participant")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Participant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long user_id;
    private Long chat_id;

    public void setId(Long id) {
    }

    public Long getUser_id() {
        return this.user_id;
    }

    public Long getChat_id() {
        return this.chat_id;
    }
}
