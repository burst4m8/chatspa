package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.Chat;
import kz.aitu.chat1906.repositories.IChatRepository;
import kz.aitu.chat1906.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chats")
public class ChatController {
    private IChatRepository chatRepository;
    private MessageService messageService;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(chatRepository.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addChat(@RequestBody Chat chat){
        chatRepository.save(chat);

        return ResponseEntity.ok("Chat added.");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateChat(@RequestBody Chat newChat, @PathVariable("id") Long id){
        newChat.setId(id);
        chatRepository.save(newChat);

        return ResponseEntity.ok("Chat updated.");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable("id") Long id){
        chatRepository.deleteById(id);

        return ResponseEntity.ok("Chat deleted.");
    }

    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId) {
        return ResponseEntity.ok(messageService.getMessagesByChat(chatId));
    }

}
