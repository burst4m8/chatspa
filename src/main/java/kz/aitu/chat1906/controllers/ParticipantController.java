package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.Message;
import kz.aitu.chat1906.models.Participant;
import kz.aitu.chat1906.repositories.IParticipantRepository;
import kz.aitu.chat1906.services.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/participants")
public class ParticipantController {
    private IParticipantRepository participantRepository;
    private ParticipantService participantService;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(participantRepository.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant){
        participantRepository.save(participant);

        return ResponseEntity.ok("Participant added.");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateParticipant(@RequestBody Participant newParticipant, @PathVariable("id") Long id){
        newParticipant.setId(id);
        participantRepository.save(newParticipant);

        return ResponseEntity.ok("Participant updated.");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteParticipant(@PathVariable("id") Long id){
        participantRepository.deleteById(id);

        return ResponseEntity.ok("Participant deleted.");
    }

    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> getUsersByChatId(@PathVariable Long chatId){
        return ResponseEntity.ok(participantService.getUsersByChat(chatId));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getChatsByUserId(@PathVariable Long userId){
        return ResponseEntity.ok(participantService.getChatsByUser(userId));
    }
}
