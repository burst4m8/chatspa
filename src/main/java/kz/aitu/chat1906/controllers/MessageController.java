package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.Message;
import kz.aitu.chat1906.repositories.IMessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/messages")
public class MessageController {
    private IMessageRepository messageRepository;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(messageRepository.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addMessage(@RequestBody Message message){
        messageRepository.save(message);

        return ResponseEntity.ok("Message added.");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateMessage(@RequestBody Message newMessage, @PathVariable("id") Long id){
        newMessage.setId(id);
        messageRepository.save(newMessage);

        return ResponseEntity.ok("Message updated.");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable("id") Long id){
        messageRepository.deleteById(id);

        return ResponseEntity.ok("Message deleted.");
    }
}
