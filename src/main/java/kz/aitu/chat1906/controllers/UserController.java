package kz.aitu.chat1906.controllers;

import kz.aitu.chat1906.models.User;
import kz.aitu.chat1906.repositories.IUserRepository;
import kz.aitu.chat1906.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {
    private IUserRepository userRepository;
    private UserService userService;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(userRepository.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<?> addUser(@RequestBody User user){
        userRepository.save(user);

        return ResponseEntity.ok("User added.");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User newUser, @PathVariable("id") Long id){
        newUser.setId(id);
        userRepository.save(newUser);

        return ResponseEntity.ok("User updated.");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
        userRepository.deleteById(id);

        return ResponseEntity.ok("User deleted.");
    }
}
