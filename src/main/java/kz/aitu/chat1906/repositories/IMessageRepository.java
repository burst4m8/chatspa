package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IMessageRepository extends JpaRepository<Message,Long> {

    List<Message> findMessagesByChatId(Long chatId);
}
