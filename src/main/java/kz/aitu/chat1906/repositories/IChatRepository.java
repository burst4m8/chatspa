package kz.aitu.chat1906.repositories;

import kz.aitu.chat1906.models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IChatRepository extends JpaRepository<Chat,Long> {
}
