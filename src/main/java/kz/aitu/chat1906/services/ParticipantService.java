package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.Chat;
import kz.aitu.chat1906.models.Participant;
import kz.aitu.chat1906.models.User;
import kz.aitu.chat1906.repositories.IChatRepository;
import kz.aitu.chat1906.repositories.IParticipantRepository;
import kz.aitu.chat1906.repositories.IUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ParticipantService {
    private IParticipantRepository participantRepository;
    private IUserRepository userRepository;
    private IChatRepository chatRepository;

    public List<User> getUsersByChat(Long chatId) {
        List<Participant> participants = participantRepository.findParticipantsByChatId(chatId);
        return participants.stream()
                .map(participant -> userRepository.findById(participant.getUser_id()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public List<Chat> getChatsByUser(Long userId) {
        List<Participant> participants = participantRepository.findParticipantByUserId(userId);
        return participants.stream()
                .map(participant -> chatRepository.findById(participant.getChat_id()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
