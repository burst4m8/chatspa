package kz.aitu.chat1906.services;

import kz.aitu.chat1906.models.Message;
import kz.aitu.chat1906.repositories.IMessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private IMessageRepository messageRepository;

    public List<Message> getMessagesByChat(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }
}

